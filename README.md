# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Conver xml files into excel offline.
* Conver xml files into Text offline
* [Demo](merge)

### How do I get set up? ###

XML To Excel:

1. Create Maven Project
2. Copy all the dependencies in pom.xml
3. Modify the code accordingly
4. Run and get Excel.

XML To Text:
1. Create Maven Project
2. Copy all the dependencies in pom.xml
3. Modify the code accordingly
4. Run and get Text.

### Who do I talk to? ###

* Let me know in case of any concerns : lakshya.arora@mtxb2b.com