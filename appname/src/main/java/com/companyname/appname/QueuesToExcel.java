package com.companyname.appname;
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook; 
import org.apache.poi.ss.usermodel.*; 

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.*; 
import java.util.*;
class QueuesToExcel {
    public static void main(String [] args){
        Workbook wb = new HSSFWorkbook(); 
        Sheet sheet1 = wb.createSheet("lwc"); 
        Map<String, Object[]> data = new TreeMap<String, Object[]>(); 
        data.put("1", new Object[]{"Name","doesSendEmailToMembers","Email","User","Public groups","QueueSObject"}); 
        int mapKey = 1;
        try {
        int count=0;
        File[] directory = new File("D:/Gus databook-20200117T090813Z-001/Gus databook/queues/").listFiles();
        for(File file : directory){
            
            String name = "";
            String email = "";
            String queueMembers = "";
            String queueSObject = "";
            String doesSendEmailToMembers ="";
            String users = "";
            String publicGroups = "";
            count++;
            System.out.println("File Name = "+file.getName());
          //  if(!file.getName().contains("cls-meta.xml") && !(file.getName().contains("_Test") || file.getName().contains("_test") || file.getName().contains("Test"))  ){
              
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();         
            NodeList nList = doc.getElementsByTagName("Queue");

                mapKey++;
                // String fileName = file.getName();
                // data.put(mapKey+"", new Object[]{fileName}); 
                // System.out.println("File Name = "+file.getName());



                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if(eElement.getElementsByTagName("name").item(0) != null)
                        name = eElement.getElementsByTagName("name").item(0).getTextContent();
                        if(eElement.getElementsByTagName("doesSendEmailToMembers").item(0) != null)
                        doesSendEmailToMembers = eElement.getElementsByTagName("doesSendEmailToMembers").item(0).getTextContent();
                        if(eElement.getElementsByTagName("email").item(0) != null)
                        email = eElement.getElementsByTagName("email").item(0).getTextContent();


                        
                        NodeList nList1 = doc.getElementsByTagName("sobjectType");
                        for(int temp1 = 0; temp1 < nList1.getLength(); temp1++){
                            Node nNode1 = nList1.item(temp1);
                            if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
                                queueSObject =queueSObject +"\n"+ nNode1.getTextContent();
                                System.out.println("Sobject of file = "+queueSObject);
                            }
                        }

                        NodeList nList2 = doc.getElementsByTagName("user");
                        for(int temp1 = 0; temp1 < nList2.getLength(); temp1++){
                            Node nNode1 = nList2.item(temp1);
                            if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
                                users =users +"\n"+ nNode1.getTextContent();
                                System.out.println("users of file = "+queueSObject);
                            }
                        }

                        NodeList nList3 = doc.getElementsByTagName("publicGroup");
                        for(int temp1 = 0; temp1 < nList3.getLength(); temp1++){
                            Node nNode1 = nList1.item(temp1);
                            if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
                                publicGroups =publicGroups +"\n"+ nNode1.getTextContent();
                                System.out.println("publicGroups of file = "+publicGroups);
                            }
                        }
                        
                        // if(eElement.getElementsByTagName("inlineHelpText").item(0) != null)
                        //   helpText =  eElement.getElementsByTagName("inlineHelpText").item(0).getTextContent();
                        // if(eElement.getElementsByTagName("type").item(0).getTextContent().equalsIgnoreCase("picklist") || eElement.getElementsByTagName("type").item(0).getTextContent().equalsIgnoreCase("multipickList")){
                        //     fieldType = eElement.getElementsByTagName("type").item(0).getTextContent();
                        //     NodeList nList1 = doc.getElementsByTagName("value");
                        //     System.out.println("Piclist total values ="+nList1.getLength());
                        //     for(int temp1 = 0; temp1 < nList1.getLength(); temp1++){
                        //         Node nNode1 = nList1.item(temp1);
                        //         if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
                        //             Element eElement1 = (Element) nNode1;  
                        //             detailsOfField =detailsOfField +"\n"+ eElement1.getElementsByTagName("fullName").item(0).getTextContent();
                        //         }
                        //     }
                        // } 
                        // else if(eElement.getElementsByTagName("formula").item(0) != null){
                        //     fieldType = "Formula(" + eElement.getElementsByTagName("type").item(0).getTextContent()+")";
                        //     anyOtherNotes = eElement.getElementsByTagName("formula").item(0).getTextContent();
                        // }
                        // else if(eElement.getElementsByTagName("referenceTo").item(0) != null)
                        // fieldType = eElement.getElementsByTagName("type").item(0).getTextContent()+"("+eElement.getElementsByTagName("referenceTo").item(0).getTextContent()+")";
                        // else
                        // fieldType = eElement.getElementsByTagName("type").item(0).getTextContent();
                        data.put(mapKey+"", new Object[]{name, doesSendEmailToMembers, email, users,publicGroups,queueSObject}); 

               
            }    
        }
         
        Set<String> keyset = data.keySet(); 
                int rownum = 0; 
                for (String key : keyset) { 
                    Row row = sheet1.createRow(rownum++); 
                    Object[] objArr = data.get(key); 
                    int cellnum = 0; 
                    for (Object obj : objArr) { 
                        Cell cell = row.createCell(cellnum++); 
                        if (obj instanceof String) 
                            cell.setCellValue((String)obj); 
                        else if (obj instanceof Integer) 
                            cell.setCellValue((Integer)obj); 
                    } 
                } 
                System.out.println("Count="+count);   
        }
    }
    
        catch(Exception e){
            e.printStackTrace();
        }
        try { 
            System.out.println("File Created Successfully");
            FileOutputStream out = new FileOutputStream(new File("QueuesFull.xlsx")); 
            wb.write(out); 
            out.close(); 
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        } 
       
    }
}