package com.companyname.appname;
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook; 
import org.apache.poi.ss.usermodel.*; 

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.*; 
import java.util.*;

public class FieldsToExcel 
{
    public static void main(String[] args)  
    throws FileNotFoundException, IOException 
    { 
      Workbook wb = new HSSFWorkbook(); 
      try {
        File[] directory = new File("D:/objects/").listFiles();
        for (File file1 : directory) {
           Sheet sheet1 = wb.createSheet(file1.getName()); 
           Map<String, Object[]> data = new TreeMap<String, Object[]>(); 
           data.put("1", new Object[]{"Field Label", "API Name", "Field Type", "Details of Field","Description","Help Text","Any Other Notes"}); 
           int mapKey = 1;
           File[] files = new File("D:/objects/"+file1.getName()+"/fields").listFiles();
           System.out.println("Files="+files);
           if(files != null){
             for (File file : files) {
                mapKey++;
                String fieldLabel="";
                String aPIName="";
                String fieldType="";
                String detailsOfField="";
                String description="";
                String helpText="";
                String anyOtherNotes="";           
                File fXmlFile = new File("D:/objects/"+file1.getName()+"/fields/"+file.getName());
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();         
                NodeList nList = doc.getElementsByTagName("CustomField");
                for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if(eElement.getElementsByTagName("label").item(0) != null)
                fieldLabel = eElement.getElementsByTagName("label").item(0).getTextContent();
                if(eElement.getElementsByTagName("fullName").item(0) != null)
                 aPIName = eElement.getElementsByTagName("fullName").item(0).getTextContent();
                if(eElement.getElementsByTagName("description").item(0) != null)
                 description = eElement.getElementsByTagName("description").item(0).getTextContent();
                if(eElement.getElementsByTagName("inlineHelpText").item(0) != null)
                  helpText =  eElement.getElementsByTagName("inlineHelpText").item(0).getTextContent();
                if(eElement.getElementsByTagName("type").item(0).getTextContent().equalsIgnoreCase("picklist") || eElement.getElementsByTagName("type").item(0).getTextContent().equalsIgnoreCase("multipickList")){
                    fieldType = eElement.getElementsByTagName("type").item(0).getTextContent();
                    NodeList nList1 = doc.getElementsByTagName("value");
                    System.out.println("Piclist total values ="+nList1.getLength());
                    for(int temp1 = 0; temp1 < nList1.getLength(); temp1++){
                        Node nNode1 = nList1.item(temp1);
                        if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement1 = (Element) nNode1;  
                            detailsOfField =detailsOfField +"\n"+ eElement1.getElementsByTagName("fullName").item(0).getTextContent();
                        }
                    }
                } 
                else if(eElement.getElementsByTagName("formula").item(0) != null){
                    fieldType = "Formula(" + eElement.getElementsByTagName("type").item(0).getTextContent()+")";
                    anyOtherNotes = eElement.getElementsByTagName("formula").item(0).getTextContent();
                }
                else if(eElement.getElementsByTagName("referenceTo").item(0) != null)
                fieldType = eElement.getElementsByTagName("type").item(0).getTextContent()+"("+eElement.getElementsByTagName("referenceTo").item(0).getTextContent()+")";
                else
                fieldType = eElement.getElementsByTagName("type").item(0).getTextContent();
                data.put(mapKey+"", new Object[]{fieldLabel, aPIName, fieldType, detailsOfField,description,helpText,anyOtherNotes}); 
                Set<String> keyset = data.keySet(); 
                int rownum = 0; 
                for (String key : keyset) { 
                    Row row = sheet1.createRow(rownum++); 
                    Object[] objArr = data.get(key); 
                    int cellnum = 0; 
                    for (Object obj : objArr) { 
                        Cell cell = row.createCell(cellnum++); 
                        if (obj instanceof String) 
                            cell.setCellValue((String)obj); 
                        else if (obj instanceof Integer) 
                            cell.setCellValue((Integer)obj); 
                    } 
                } 
            }
        }   
              } 
            }
        

           
            try { 
                FileOutputStream out = new FileOutputStream(new File("FieldXmlToExcel.xlsx")); 
                wb.write(out); 
                out.close(); 
            } 
            catch (Exception e) { 
                e.printStackTrace(); 
            } 
        }
       } 
       catch (Exception e) {
          e.printStackTrace();
       }
     System.out.println("Sheets Has been Created successfully"); 
     wb.close();
  } 
}
