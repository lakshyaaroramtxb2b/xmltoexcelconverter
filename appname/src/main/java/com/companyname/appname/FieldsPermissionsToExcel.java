package com.companyname.appname;
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook; 
import org.apache.poi.ss.usermodel.*; 

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.*; 
import java.util.*;
class ProfileFieldsPermissionsToExcel {
    public static void main(String [] args){
        Workbook wb = new HSSFWorkbook(); 
        Sheet sheet1 = wb.createSheet("FieldsPermission"); 
        Map<String, Object[]> data = new TreeMap<String, Object[]>(); 
        data.put("1", new Object[]{"Profile Name","Object Name","Field Name","Readable","Editable"}); 
        int mapKey = 1;
        try {
        int count=0;
        File[] directory = new File("D:/7SHGSE").listFiles();
        for(File file : directory){
            
            String profileName = "";
            String objectName = "";
            String fieldName = "";
            String readable = "";
            String editable = "";
        
            count++;
            System.out.println("File Name = "+file.getName());
          //  if(!file.getName().contains("cls-meta.xml") && !(file.getName().contains("_Test") || file.getName().contains("_test") || file.getName().contains("Test"))  ){
            profileName = file.getName();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();         
            NodeList nList = doc.getElementsByTagName("fieldPermissions");

               
                // String fileName = file.getName();
                // data.put(mapKey+"", new Object[]{fileName}); 
                // System.out.println("File Name = "+file.getName());



                for (int temp = 0; temp < nList.getLength(); temp++) {
                    mapKey++;
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if(eElement.getElementsByTagName("editable").item(0) != null)
                        editable = eElement.getElementsByTagName("editable").item(0).getTextContent();
                        if(eElement.getElementsByTagName("readable").item(0) != null)
                        readable = eElement.getElementsByTagName("readable").item(0).getTextContent();
                        
                        if(eElement.getElementsByTagName("field").item(0) != null){
                            String fieldElement = eElement.getElementsByTagName("field").item(0).getTextContent();
                            String []fieldElementArray = fieldElement.split("\\.");
                            objectName = fieldElementArray[0];
                            fieldName = fieldElementArray[1];
                        }
                        if(objectName.equals("Opportunity"))
                        data.put(mapKey+"", new Object[]{profileName,objectName,fieldName,readable,editable}); 

               
            }    
        }
         
        Set<String> keyset = data.keySet(); 
                int rownum = 0; 
                for (String key : keyset) { 
                    Row row = sheet1.createRow(rownum++); 
                    Object[] objArr = data.get(key); 
                    int cellnum = 0; 
                    for (Object obj : objArr) { 
                        Cell cell = row.createCell(cellnum++); 
                        if (obj instanceof String) 
                            cell.setCellValue((String)obj); 
                        else if (obj instanceof Integer) 
                            cell.setCellValue((Integer)obj); 
                    } 
                } 
                System.out.println("Count="+count);   
        }
    }
    
        catch(Exception e){
            e.printStackTrace();
        }
        try { 
            System.out.println("File Created Successfully");
            FileOutputStream out = new FileOutputStream(new File("ProfileFieldPermission.xlsx")); 
            wb.write(out); 
            out.close(); 
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        } 
       
    }
}