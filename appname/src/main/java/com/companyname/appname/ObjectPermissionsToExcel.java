package com.companyname.appname;
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook; 
import org.apache.poi.ss.usermodel.*; 

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.*; 
import java.util.*;
class ObjectPermissionsToExcel {
    public static void main(String [] args){
        Workbook wb = new HSSFWorkbook(); 
        Sheet sheet1 = wb.createSheet("ObjectPermissionsToExcel"); 
        Map<String, Object[]> data = new TreeMap<String, Object[]>(); 
        data.put("1", new Object[]{"Profile Name","Object Name","Read","Create","Edit","Delete","ViewAll","ModifyAll"}); 
        int mapKey = 1;
        try {
        int count=0;
        File[] directory = new File("D:/Gus databook-20200117T090813Z-001/Gus databook/profilespermsets/").listFiles();
        for(File file : directory){
            
            String profileName = "";
            String objectName = "";
            String read = "";
            String create = "";
            String edit = "";
            String delete = "";
            String viewAll = "";
            String modifyAll = "";
        
            count++;
            System.out.println("File Name = "+file.getName());
          //  if(!file.getName().contains("cls-meta.xml") && !(file.getName().contains("_Test") || file.getName().contains("_test") || file.getName().contains("Test"))  ){
            profileName = file.getName();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();         
            NodeList nList = doc.getElementsByTagName("objectPermissions");

               
                // String fileName = file.getName();
                // data.put(mapKey+"", new Object[]{fileName}); 
                // System.out.println("File Name = "+file.getName());



                for (int temp = 0; temp < nList.getLength(); temp++) {
                    mapKey++;
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if(eElement.getElementsByTagName("object").item(0) != null)
                        objectName = eElement.getElementsByTagName("object").item(0).getTextContent();
                        if(eElement.getElementsByTagName("allowCreate").item(0) != null)
                        create = eElement.getElementsByTagName("allowCreate").item(0).getTextContent();
                        if(eElement.getElementsByTagName("allowDelete").item(0) != null)
                        delete = eElement.getElementsByTagName("allowDelete").item(0).getTextContent();
                        if(eElement.getElementsByTagName("allowEdit").item(0) != null)
                        edit = eElement.getElementsByTagName("allowEdit").item(0).getTextContent();
                        if(eElement.getElementsByTagName("allowRead").item(0) != null)
                        read = eElement.getElementsByTagName("allowRead").item(0).getTextContent();
                        if(eElement.getElementsByTagName("modifyAllRecords").item(0) != null)
                        modifyAll = eElement.getElementsByTagName("modifyAllRecords").item(0).getTextContent();
                        if(eElement.getElementsByTagName("viewAllRecords").item(0) != null)
                        viewAll = eElement.getElementsByTagName("viewAllRecords").item(0).getTextContent();
                        
                       
                        data.put(mapKey+"", new Object[]{profileName,objectName,read,create,edit,delete,viewAll,modifyAll}); 

               
            }    
        }
         
        Set<String> keyset = data.keySet(); 
                int rownum = 0; 
                for (String key : keyset) { 
                    Row row = sheet1.createRow(rownum++); 
                    Object[] objArr = data.get(key); 
                    int cellnum = 0; 
                    for (Object obj : objArr) { 
                        Cell cell = row.createCell(cellnum++); 
                        if (obj instanceof String) 
                            cell.setCellValue((String)obj); 
                        else if (obj instanceof Integer) 
                            cell.setCellValue((Integer)obj); 
                    } 
                } 
                System.out.println("Count="+count);   
        }
    }
    
        catch(Exception e){
            e.printStackTrace();
        }
        try { 
            System.out.println("File Created Successfully");
            FileOutputStream out = new FileOutputStream(new File("ObjectPermissionsToExcel.xlsx")); 
            wb.write(out); 
            out.close(); 
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        } 
       
    }
}